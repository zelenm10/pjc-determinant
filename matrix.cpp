#include <random>
#include <fstream>
#include <iterator>
#include <random>
#include <vector>
#include <iostream>
#include <stdlib.h>
#include <iomanip>
#include <stdio.h>


using namespace std;

//generate random data
double getRandomDouble(int min, int max) {
    static minstd_rand mt{ random_device{}() };
    static uniform_real_distribution<> dist(-min, max);
    return dist(mt);
}

vector<vector<double>> getRandomMatrix(size_t size) {
    vector<vector<double>> data;
    
    for (size_t i = 0; i < size; i++) {
        vector<double> row;

        for (size_t l = 0; l < size; l++) {
            double value = getRandomDouble(10, 10);
            row.push_back(value);
        }

        data.push_back(row);
    }
    return data;
}

//print data
void print(vector<vector<double>> &data) {
    for (size_t i = 0; i < data.size(); ++i) {
        for (size_t j = 0; j < data.size(); ++j) {
            cout << setw(12) << data[i][j] << setw(12);
        }
        cout << endl << endl;
    }
    cout << endl;
}

//find max number in row (pivot) for gem
int find_pivot(vector<vector<double>> &data, int size, int k) {
    float max = data[k][k];
    int index = k;
    for (int i = k + 1; i < size; i++) {
        if (data[i][k] > max) {
            max = data[i][k];
            index = i;
        }
    }
    return index;
}

//gauss elimination method
bool gem(vector<vector<double>> &data) {
    int size = data.size();
    int sign = 0;
    for (int k = 0; k < size; k++) {
        int pivot = find_pivot(data, size, k);
        if (pivot != k) {
            swap(data[pivot], data[k]);
            sign++;
        }
        for (int i = k + 1; i < size; i++) {
            if (data[i][k] != 0) {
                double l = data[i][k] / data[k][k];
                data[i][k] = 0;
                for (int j = k + 1; j < size; j++) {
                    data[i][j] -= l * data[k][j];
                }
            }
        }
    }
    //return false if sign is + 
    return sign % 2 != 0;
}

long double count_determinant(vector<vector<double>> &data) {
    long double determinant = 1;
    int size = data.size();

    if ( gem(data) ) {
        //sign of determinant from gem
        determinant *= -1;
    }

    for (int k = 0; k < size; k++) {
        determinant *= data[k][k];
    }
    return determinant;
}


int main () {
    
    //test for 2x2 matrix
    vector<vector<double>> data2 = getRandomMatrix(2);
    cout << "print data 2x2:" << endl;
    print(data2);
    cout << "det 2x2: " << count_determinant(data2) << endl;
    cout << endl;

    //test for 3x3 matrix
    vector<vector<double>> data3 = getRandomMatrix(3);
    cout << "print data 3x3:" << endl;
    print(data3);
    cout << "det 3x3: " << count_determinant(data3) << endl;
    cout << endl;

    //test for 15x15 matrix
    vector<vector<double>> data15 = getRandomMatrix(15);
    cout << "print data 15x15:" << endl;
    print(data15);
    cout << "det 15x15: " << count_determinant(data15) << endl;
    cout << endl;

    //test for 100x100 matrix
    vector<vector<double>> data100 = getRandomMatrix(100);
    cout << "det 100x100: " << count_determinant(data100) << endl;
    cout << endl;

    //test for 1000x1000 matrix
    vector<vector<double>> data1000 = getRandomMatrix(1000);
    cout << "det 1000x1000: " << count_determinant(data1000) << endl;
    

}