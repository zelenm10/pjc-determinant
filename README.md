**Vypocet**

Gaussova eliminacni metoda: uprava matice na horni blokovy tvar, soucasne spocitame znamenko determinantu (podle poctu uprav gem)

Výpočet determinantu z upravene matice: determinant spocitame pronasobenim cisel na diagonale


**Implementace**

* Pomoci metody getRandomMatrix() vygenerujeme nahodnou ctvercovou matici o zadanem rozmeru.
* Pomoci metody print() si muzeme matici vypsat.
* Pomoci metody find_pivot() nalezneme nejvetsi cislo v radku pro vypocet GEM.
* Metoda gem() nam upravi matici na horni blokovy tvar a spocita znamenko determinantu.
* Metoda cout_determinant() spocita determinant matice.


**Spusteni**

`g++ -std=c++14 matrix.cpp`


`./a.out`